library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.std_logic_unsigned.all;
use IEEE.std_logic_arith.all;


entity tb is
end tb;

architecture behavioral of tb is

  component I2C_EEPROM is 
   generic (device : string(1 to 5) := "24C16");  
   port ( 
    STRETCH            : IN    time := 1 ns;      
    E0                 : IN    std_logic := 'L';  
    E1                 : IN    std_logic := 'L';  
    E2                 : IN    std_logic := 'L';  
    WC                 : IN    std_logic := 'L';  
    SCL                : INOUT std_logic; 
    SDA                : INOUT std_logic 
  ); 
  END component I2C_EEPROM; 

  component i2c_master_eeprom is
    port (
      clk : in std_logic;
      rst : in std_logic;
      nReset : in std_logic;
      start_wr_i : in std_logic;
			start_rd_i : in std_logic;
      addr_i : in std_logic_vector(7 downto 0);
      data_i : in std_logic_vector(7 downto 0);
      done_o : out std_logic;
      sda_i : in std_logic;
      sda_o : out std_logic;
      sda_oen : out std_logic;
      scl_i : in std_logic;
      scl_o : out std_logic;
      scl_oen : out std_logic
      );
  end component i2c_master_eeprom;

  signal   SCL     : std_logic;
  signal   SDA     : std_logic;
  
  signal   clk        : std_logic := '0';
  signal   rst        : std_logic := '1';
  signal   nReset     : std_logic := '0';
  signal   start_wr_i : std_logic := '0';
  signal   start_rd_i : std_logic := '0';
  signal   addr_i     : std_logic_vector(7 downto 0) := (others => '0');
  signal   data_i     : std_logic_vector(7 downto 0) := (others => '0');
  signal   done_o     : std_logic;
  signal   sda_i      : std_logic := '1';
  signal   sda_o      : std_logic;
  signal   sda_oen    : std_logic;
  signal   scl_i      : std_logic := '1';
  signal   scl_o      : std_logic;
  signal   scl_oen    : std_logic;
    
begin

  clk <= not clk after (0.50 * 1000.0 ns /  50.0);

  p_nReset: process 
  begin
    nReset <= '0';
    rst <= '1';
    wait for 100 ns;
    nReset <= '1';
    wait for 100 ns;
    rst <= '0';
    wait;
  end process;
    
  p_main_write: process 
  begin
		wait for 1000 ns;
		start_wr_i <= '1';
		addr_i <= x"20";
    data_i <= x"AE";
		
		
  	wait until done_o = '1';
		start_wr_i <= '0';
		
    wait for 1000 ns;
		addr_i <= x"20";
		start_rd_i <= '1';
		
	 
	 wait until done_o = '1';
	  start_rd_i <= '0';
		
	wait;
  end process;
  


  eeprom : I2C_EEPROM
  generic map (
    device => "24C02"
  )
  port map (
    -- STRETCH => STRETCH,
    -- E0      => E0,
    -- E1      => E1,
    -- E2      => E2,
    -- WC      => WC,
    SCL     => SCL,
    SDA     => SDA
  );

  i2c : i2c_master_eeprom
  port map (
    clk        => clk,
    rst        => rst,
    nReset     => nReset,
    start_wr_i => start_wr_i,
		start_rd_i => start_rd_i,
    addr_i     => addr_i,
    data_i     => data_i,
    done_o     => done_o,
    sda_i      => sda_i,
    sda_o      => sda_o,
    sda_oen    => sda_oen,
    scl_i      => scl_i,
    scl_o      => scl_o,
    scl_oen    => scl_oen
  );
  
  
  SCL <= scl_o when scl_oen = '0' else 'H';
  SDA <= sda_o when sda_oen = '0' else 'H';
  scl_i <=  '0' when SCL = '0' else '1';
  sda_i <= '0' when SDA = '0' else '1';

end behavioral;
