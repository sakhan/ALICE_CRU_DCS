onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider tb/*
add wave -noupdate -radix hexadecimal /tb/SCL
add wave -noupdate -radix hexadecimal /tb/SDA
add wave -noupdate -radix hexadecimal /tb/clk
add wave -noupdate -radix hexadecimal /tb/rst
add wave -noupdate -radix hexadecimal /tb/nReset
add wave -noupdate -radix hexadecimal /tb/start_wr_i
add wave -noupdate -radix hexadecimal /tb/start_rd_i
add wave -noupdate -radix hexadecimal /tb/addr_i
add wave -noupdate -radix hexadecimal /tb/data_i
add wave -noupdate -radix hexadecimal /tb/done_o
add wave -noupdate -radix hexadecimal /tb/sda_i
add wave -noupdate -radix hexadecimal /tb/sda_o
add wave -noupdate -radix hexadecimal /tb/sda_oen
add wave -noupdate -radix hexadecimal /tb/scl_i
add wave -noupdate -radix hexadecimal /tb/scl_o
add wave -noupdate -radix hexadecimal /tb/scl_oen
add wave -noupdate -divider tb/i2c/*
add wave -noupdate -radix hexadecimal /tb/i2c/clk
add wave -noupdate -radix hexadecimal /tb/i2c/rst
add wave -noupdate -radix hexadecimal /tb/i2c/nReset
add wave -noupdate -radix hexadecimal /tb/i2c/start_wr_i
add wave -noupdate -radix hexadecimal /tb/i2c/addr_i
add wave -noupdate -radix hexadecimal /tb/i2c/data_i
add wave -noupdate -radix hexadecimal /tb/i2c/done_o
add wave -noupdate -radix hexadecimal /tb/i2c/start_rd_i
add wave -noupdate -radix hexadecimal /tb/i2c/start
add wave -noupdate -radix hexadecimal /tb/i2c/stop
add wave -noupdate -radix hexadecimal /tb/i2c/data_o
add wave -noupdate -radix hexadecimal /tb/i2c/sda_i
add wave -noupdate -radix hexadecimal /tb/i2c/sda_o
add wave -noupdate -radix hexadecimal /tb/i2c/sda_oen
add wave -noupdate -radix hexadecimal /tb/i2c/scl_i
add wave -noupdate -radix hexadecimal /tb/i2c/scl_o
add wave -noupdate -radix hexadecimal /tb/i2c/scl_oen
add wave -noupdate -radix hexadecimal /tb/i2c/core_rst
add wave -noupdate -radix hexadecimal /tb/i2c/core_en
add wave -noupdate -radix hexadecimal /tb/i2c/rd
add wave -noupdate -radix hexadecimal /tb/i2c/wr
add wave -noupdate -radix hexadecimal /tb/i2c/ack_in
add wave -noupdate -radix hexadecimal /tb/i2c/din
add wave -noupdate -radix hexadecimal /tb/i2c/dout
add wave -noupdate -radix hexadecimal /tb/i2c/dout_ld
add wave -noupdate -radix hexadecimal /tb/i2c/i2c_done
add wave -noupdate -radix hexadecimal /tb/i2c/ack_out
add wave -noupdate -radix hexadecimal /tb/i2c/i2c_al
add wave -noupdate -radix hexadecimal /tb/i2c/sm_state
add wave -noupdate -radix hexadecimal /tb/i2c/count
add wave -noupdate -radix hexadecimal /tb/i2c/index_add
add wave -noupdate -radix hexadecimal /tb/i2c/read_qsfp_reg
add wave -noupdate -radix hexadecimal /tb/i2c/read_qsfp_reg_done
add wave -noupdate -radix hexadecimal /tb/i2c/optical_power
add wave -noupdate -radix hexadecimal /tb/i2c/state
add wave -noupdate -radix hexadecimal /tb/i2c/dout_ld_cs
add wave -noupdate -radix hexadecimal /tb/i2c/wr_cs
add wave -noupdate -radix hexadecimal /tb/i2c/i2c_done_cs
add wave -noupdate -radix hexadecimal /tb/i2c/i2c_al_cs
add wave -noupdate -radix hexadecimal /tb/i2c/ack_out_cs

add wave -noupdate -radix hexadecimal /tb/i2c/din_cs
add wave -noupdate -radix hexadecimal /tb/i2c/sm_state_cs
add wave -noupdate -divider tb/i2c/i_i2c_master_byte_ctrl/bit_ctrl/*
add wave -noupdate -radix hexadecimal /tb/i2c/i_i2c_master_byte_ctrl/bit_ctrl/clk
add wave -noupdate -radix hexadecimal /tb/i2c/i_i2c_master_byte_ctrl/bit_ctrl/rst
add wave -noupdate -radix hexadecimal /tb/i2c/i_i2c_master_byte_ctrl/bit_ctrl/nReset
add wave -noupdate -radix hexadecimal /tb/i2c/i_i2c_master_byte_ctrl/bit_ctrl/ena
add wave -noupdate -radix hexadecimal /tb/i2c/i_i2c_master_byte_ctrl/bit_ctrl/clk_cnt
add wave -noupdate -radix hexadecimal /tb/i2c/i_i2c_master_byte_ctrl/bit_ctrl/cmd
add wave -noupdate -radix hexadecimal /tb/i2c/i_i2c_master_byte_ctrl/bit_ctrl/cmd_ack
add wave -noupdate -radix hexadecimal /tb/i2c/i_i2c_master_byte_ctrl/bit_ctrl/busy
add wave -noupdate -radix hexadecimal /tb/i2c/i_i2c_master_byte_ctrl/bit_ctrl/al
add wave -noupdate -radix hexadecimal /tb/i2c/i_i2c_master_byte_ctrl/bit_ctrl/din
add wave -noupdate -radix hexadecimal /tb/i2c/i_i2c_master_byte_ctrl/bit_ctrl/dout

add wave -noupdate -radix hexadecimal /tb/i2c/i_i2c_master_byte_ctrl/bit_ctrl/scl_i
add wave -noupdate -radix hexadecimal /tb/i2c/i_i2c_master_byte_ctrl/bit_ctrl/scl_o
add wave -noupdate -radix hexadecimal /tb/i2c/i_i2c_master_byte_ctrl/bit_ctrl/scl_oen
add wave -noupdate -radix hexadecimal /tb/i2c/i_i2c_master_byte_ctrl/bit_ctrl/sda_i
add wave -noupdate -radix hexadecimal /tb/i2c/i_i2c_master_byte_ctrl/bit_ctrl/sda_o
add wave -noupdate -radix hexadecimal /tb/i2c/i_i2c_master_byte_ctrl/bit_ctrl/sda_oen
add wave -noupdate -radix hexadecimal /tb/i2c/i_i2c_master_byte_ctrl/bit_ctrl/c_state
add wave -noupdate -radix hexadecimal /tb/i2c/i_i2c_master_byte_ctrl/bit_ctrl/iscl_oen
add wave -noupdate -radix hexadecimal /tb/i2c/i_i2c_master_byte_ctrl/bit_ctrl/isda_oen
add wave -noupdate -radix hexadecimal /tb/i2c/i_i2c_master_byte_ctrl/bit_ctrl/sda_chk
add wave -noupdate -radix hexadecimal /tb/i2c/i_i2c_master_byte_ctrl/bit_ctrl/dscl_oen
add wave -noupdate -radix hexadecimal /tb/i2c/i_i2c_master_byte_ctrl/bit_ctrl/sSCL
add wave -noupdate -radix hexadecimal /tb/i2c/i_i2c_master_byte_ctrl/bit_ctrl/sSDA
add wave -noupdate -radix hexadecimal /tb/i2c/i_i2c_master_byte_ctrl/bit_ctrl/dSCL
add wave -noupdate -radix hexadecimal /tb/i2c/i_i2c_master_byte_ctrl/bit_ctrl/dSDA
add wave -noupdate -radix hexadecimal /tb/i2c/i_i2c_master_byte_ctrl/bit_ctrl/clk_en
add wave -noupdate -radix hexadecimal /tb/i2c/i_i2c_master_byte_ctrl/bit_ctrl/scl_sync
add wave -noupdate -radix hexadecimal /tb/i2c/i_i2c_master_byte_ctrl/bit_ctrl/slave_wait
add wave -noupdate -radix hexadecimal /tb/i2c/i_i2c_master_byte_ctrl/bit_ctrl/ial
add wave -noupdate -radix hexadecimal /tb/i2c/i_i2c_master_byte_ctrl/bit_ctrl/cnt
add wave -noupdate -divider tb/eeprom/*
add wave -noupdate -radix hexadecimal /tb/eeprom/STRETCH
add wave -noupdate -radix hexadecimal /tb/eeprom/E0
add wave -noupdate -radix hexadecimal /tb/eeprom/E1
add wave -noupdate -radix hexadecimal /tb/eeprom/E2
add wave -noupdate -radix hexadecimal /tb/eeprom/WC
add wave -noupdate -radix hexadecimal /tb/eeprom/SCL
add wave -noupdate -radix hexadecimal /tb/eeprom/SDA
add wave -noupdate -radix hexadecimal /tb/eeprom/ADDR_WORD
add wave -noupdate -radix hexadecimal /tb/eeprom/BIT_PTR
add wave -noupdate -radix hexadecimal /tb/eeprom/MEM_ADDR
add wave -noupdate -radix hexadecimal /tb/eeprom/MEM_DATA
add wave -noupdate -radix hexadecimal /tb/eeprom/THIS_STATE
add wave -noupdate -radix hexadecimal /tb/eeprom/NEXT_STATE
add wave -noupdate -radix hexadecimal /tb/eeprom/SCL_IN
add wave -noupdate -radix hexadecimal /tb/eeprom/SCL_OLD
add wave -noupdate -radix hexadecimal /tb/eeprom/SCL_OUT
add wave -noupdate -radix hexadecimal /tb/eeprom/SDA_IN
add wave -noupdate -radix hexadecimal /tb/eeprom/SDA_OUT
add wave -noupdate -radix hexadecimal /tb/eeprom/START_DET
add wave -noupdate -radix hexadecimal /tb/eeprom/STOP_DET
add wave -noupdate -radix hexadecimal /tb/eeprom/DEVICE_SEL
add wave -noupdate -radix hexadecimal /tb/eeprom/RD_MODE
add wave -noupdate -radix hexadecimal /tb/eeprom/WRITE_EN
add wave -noupdate -radix hexadecimal /tb/eeprom/RECV_BYTE
add wave -noupdate -radix hexadecimal /tb/eeprom/XMIT_BYTE
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {163180970149 fs} 0}
quietly wave cursor active 1
configure wave -namecolwidth 172
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits fs
update
WaveRestoreZoom {0 fs} {205690298507 fs}
