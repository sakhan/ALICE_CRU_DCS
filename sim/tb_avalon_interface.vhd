-- av_writedata sequence is changed in this testbench, 
--  it is as per according the input done from the source of ISSP
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.std_logic_unsigned.all;
use IEEE.std_logic_arith.all;


entity tb_avalon_interface is
end entity tb_avalon_interface;

architecture tb of tb_avalon_interface is

component I2C_EEPROM is 
   generic (device : string(1 to 5) := "24C16");  
   port ( 
    STRETCH            : IN    time := 1 ns;      
    E0                 : IN    std_logic := 'L';  
    E1                 : IN    std_logic := 'L';  
    E2                 : IN    std_logic := 'L';  
    WC                 : IN    std_logic := 'L';  
    SCL                : INOUT std_logic; 
    SDA                : INOUT std_logic 
  ); 
  END component I2C_EEPROM; 



--! Component declaration for avalon_interface
component avalon_interface is

port
 (
  av_clk        : in std_logic;
  av_reset      : in std_logic ;
  nReset        : out std_logic;
  av_write      : in std_logic ;
  av_writedata  : in std_logic_vector(31 downto 0) ;
  av_read       : in std_logic ;
  av_readdata   : out std_logic_vector(31 downto 0) ;
  av_address    : in std_logic_vector(7 downto 0) ;
  av_valid      : out std_logic;
  sda_i         : in std_logic;
  sda_o         : out std_logic;
  sda_oen       : out std_logic;
  scl_i         : in std_logic;
  scl_o         : out std_logic;
  scl_oen       : out std_logic
 
  );
  end component avalon_interface;
  
  -- Signal declarations
signal   av_clk       : std_logic;
signal   av_reset     : std_logic;
signal   nReset       : std_logic;

signal   av_write     : std_logic;
signal   av_writedata : std_logic_vector(31 downto 0);
signal   av_read      : std_logic := '0';
signal   av_readdata  : std_logic_vector(31 downto 0);
signal   av_address   : std_logic_vector(7 downto 0);
signal   av_valid     : std_logic := '1';

signal   SCL          : std_logic;
signal   SDA          : std_logic;
signal   sda_i        : std_logic;
signal   sda_o        : std_logic;
signal   sda_oen      : std_logic;
signal   scl_i        : std_logic;
signal   scl_o        : std_logic;
signal   scl_oen      : std_logic;
constant clk_period : time := 20 ns;


begin
--av_clk <= not av_clk after (0.50 * 1000.0 ns / 50.0);
clk_process :process
   begin
        av_clk <= '0';
        wait for clk_period/2;  --for 0.5 ns signal is '0'.
        av_clk <= '1';
        wait for clk_period/2;  --for next 0.5 ns signal is '1'.
   end process;

 p_Reset: process 
  begin
    av_reset <= '1';
    wait for 100 ns;
    av_reset <= '0';
    wait;
  end process;
  
  p_main_write: process 
  begin
    wait for 1000 ns;
    -- entered the arbitrary data 32 bit here 
        
    wait until falling_edge(av_clk);
    av_address    <= x"00";
    av_writedata  <= x"0102BEA0";
    av_write      <= '1';
    wait until falling_edge(av_clk);
    av_write      <= '0';

    wait for 40 us;   
      
    wait until falling_edge(av_clk);
    av_address    <= x"00";
    av_writedata  <= x"020200A0";
    av_write      <= '1';
    wait until falling_edge(av_clk);
    av_write      <= '0';

    wait for 40 us;   
    
    wait until falling_edge(av_clk);
    av_address    <= x"00";
    av_read       <= '1';   
    wait until falling_edge(av_clk);
    av_read       <= '0';

    wait for 5 us;   
    
    wait until falling_edge(av_clk);
    av_address    <= x"00";
    av_read       <= '1';   
    wait until falling_edge(av_clk);
    av_read       <= '0';

    
  wait;
end process;
  

  eeprom : I2C_EEPROM
    generic map (
      device => "24C02"
    )
    port map (
        -- STRETCH => STRETCH,
        -- E0      => E0,
        -- E1      => E1,
        -- E2      => E2,
        -- WC      => WC,
      SCL     => SCL,
      SDA     => SDA
    );

   --! Port map declaration for avalon_interface
   comp_avalon_interface : avalon_interface
     port map (
                av_clk       => av_clk,
                av_reset     => av_reset,
              --  nReset       => nReset,
                
                av_write     => av_write,
                av_writedata => av_writedata,
                av_read      => av_read,
                av_readdata  => av_readdata,
                av_address   => av_address,
                av_valid     => av_valid,
                
                sda_i        => sda_i,
                sda_o        => sda_o,
                sda_oen      => sda_oen,
                scl_i        => scl_i,
                scl_o        => scl_o,
                scl_oen      => scl_oen
   );
   
      SCL   <= scl_o when scl_oen = '0' else 'H';
      SDA   <= sda_o when sda_oen = '0' else 'H';
      scl_i <=  '0'  when SCL     = '0' else '1';
      sda_i <= '0'   when SDA     = '0' else '1';
   
end architecture tb;