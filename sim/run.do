vlib work


vcom ../src/i2c_master_bit_ctrl.vhd
vcom ../src/i2c_master_byte_ctrl.vhd
vcom ../src/i2c_master_eeprom.vhd
vcom ../src/avalon_interface.vhd

vcom i2c_eeprom.vhd
vcom tb_avalon_interface.vhd

vsim -novopt -t 1fs work.tb_avalon_interface

config wave -signalnamewidth 1

add wave -noupdate -divider tb_avalon_interface/*
add wave -radix hexadecimal /tb_avalon_interface/*

add wave -noupdate -divider tb_avalon_interface/comp_avalon_interface/*
add wave -radix hexadecimal /tb_avalon_interface/comp_avalon_interface/*

add wave -noupdate -divider tb_avalon_interface/comp_avalon_interface/i2c_master/*
add wave -radix hexadecimal /tb_avalon_interface/comp_avalon_interface/i2c_master/*

add wave -noupdate -divider tb_avalon_interface/comp_avalon_interface/i2c_master/i_i2c_master_byte_ctrl/*
add wave -radix hexadecimal /tb_avalon_interface/comp_avalon_interface/i2c_master/i_i2c_master_byte_ctrl/*

add wave -noupdate -divider tb_avalon_interface/comp_avalon_interface/i2c_master/i_i2c_master_byte_ctrl/bit_ctrl/*
add wave -radix hexadecimal /tb_avalon_interface/comp_avalon_interface/i2c_master/i_i2c_master_byte_ctrl/bit_ctrl/*


add wave -noupdate -divider tb_avalon_interface/eeprom/*
add wave -radix hexadecimal /tb_avalon_interface/eeprom/*

run 200 us
