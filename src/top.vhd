library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
                                        
entity top is
port (
	CLOCK_50  : in    std_logic;                      -- input        CLOCK_50;
	LED       : out   std_logic_vector(7 downto 0);   -- output [7:0] LED;
	KEY       : in    std_logic_vector(1 downto 0);   -- input  [1:0] KEY;
	SW        : in    std_logic_vector(3 downto 0);   -- input  [3:0] SW;
	GPIO_0_D  : inout std_logic_vector(33 downto 0);  -- inout [33:0] GPIO_0_D;
	GPIO_1_D  : inout std_logic_vector(33 downto 0);   -- inout [33:0] GPIO_1_D;
  I2C_SCLK  : inout std_logic;
  I2C_SDAT  : inout std_logic
);
end top;

architecture rtl of top is

	component jtag_avalon is
		port (
			clk_clk                  : in  std_logic              						 ;             -- clk
			av_master0_waitrequest   : in  std_logic                     := '0';             -- waitrequest
			av_master0_readdata      : in  std_logic_vector(31 downto 0) ; -- readdata
			av_master0_readdatavalid : in  std_logic ;                 --   := '1';             -- readdatavalid
		--av_master0_burstcount    : out std_logic_vector(0 downto 0);                     -- burstcount
			av_master0_writedata     : out std_logic_vector(31 downto 0);                    -- writedata
			av_master0_address       : out std_logic_vector(7 downto 0);                     -- address
			av_master0_write         : out std_logic;                                        -- write
			av_master0_read          : out std_logic;                                        -- read
		--av_master0_byteenable    : out std_logic_vector(3 downto 0);                     -- byteenable
		--av_master0_debugaccess   : out std_logic;                                        -- debugaccess
			reset_reset_n            : in  std_logic                     := '1'              -- reset_n
		);
	end component jtag_avalon;
		
	
	signal clk_clk                 	 	: std_logic  ;             -- clk
	signal av_master0_waitrequest  	  : std_logic;             -- waitrequest
	signal av_master0_readdata     	  : std_logic_vector(31 downto 0) ; 
	signal av_master0_readdatavalid	  : std_logic ;             
	signal av_master0_writedata    	  : std_logic_vector(31 downto 0);                    
	signal av_master0_address      	  : std_logic_vector(7 downto 0);                     
	signal av_master0_write        	  : std_logic;                                        
	signal av_master0_read         	  : std_logic;                                        
	signal reset_reset_n           	  : std_logic;  
	
	component avalon_interface is
	port
 ( 
	--================
    -- CLK & RST
  --================	
	av_clk				: in std_logic;
	av_reset			: in std_logic ;
  nReset				: in std_logic;
	--===============================================
    -- Avalon Interface (Memory Mapped Slave)
  --=============================================== 
  av_write			: in std_logic ;
  av_writedata	: in std_logic_vector(31 downto 0) ;
	av_read				: in std_logic ;
  av_readdata		: out std_logic_vector(31 downto 0) ;
  av_address		: in std_logic_vector(7 downto 0) ;
	av_valid			:	out std_logic;
  av_waitrequest: out std_logic;
	--===================
    -- I2C interface
  --===================
	sda_i 				: in std_logic;
  sda_o 				: out std_logic;
  sda_oen 			: out std_logic;
  scl_i 				: in std_logic;
  scl_o 				: out std_logic;
  scl_oen 			: out std_logic
 
  );
  end component avalon_interface;
	
	signal av_clk			    : std_logic;
	signal av_reset			  :	std_logic;	
	signal av_write			  :	std_logic;
	signal nReset				  :	std_logic;
	signal av_writedata	  :	std_logic_vector(31 downto 0);
	signal av_read			  :	std_logic;			
	signal av_readdata	  :	std_logic_vector(31 downto 0);	
	signal av_address		  :	std_logic_vector(7 downto 0) ;
  signal av_valid       : std_logic;
  signal av_waitrequest :  std_logic;
	signal sda_i 				  : std_logic;
	signal sda_o 	        :	std_logic;
	signal sda_oen        :	std_logic;
	signal scl_i 				  : std_logic;
	signal scl_o 	        :	std_logic;
	signal scl_oen        :	std_logic;

  
begin
		comp_jtag_avalon: jtag_avalon
			port map (
				
				 clk_clk                 		=>		 CLOCK_50, 
				 reset_reset_n           	  =>     '1' , 
				 av_master0_waitrequest  	  =>     av_master0_waitrequest , 	
				 av_master0_readdata     	  =>     av_master0_readdata ,    	
         av_master0_readdatavalid	  =>     av_master0_readdatavalid	,
				 av_master0_writedata    	  =>     av_master0_writedata,    	
				 av_master0_address      	  =>     av_master0_address,      	
				 av_master0_write        	  =>     av_master0_write,        	
				 av_master0_read         	  =>     av_master0_read         									
			);
	
		avalon :	avalon_interface
			port map (
			
				av_clk				=>		CLOCK_50,			
				av_reset			=>    '0',		
				nReset				=>    '1',			
				av_write			=>    av_write,		
				av_writedata	=>    av_writedata,
				av_read				=>    av_read,			
				av_readdata		=>    av_readdata,	
				av_address		=>    av_address,
        av_valid  		=>    av_valid,
        av_waitrequest=>    av_waitrequest,
				sda_i 				=>    sda_i, 			
				sda_o 				=>    sda_o, 			
				sda_oen 			=>    sda_oen, 		
				scl_i 				=>    scl_i, 			
				scl_o 				=>    scl_o, 			
				scl_oen 			=>    scl_oen 		
	      
				);
			
	av_clk							      <=		clk_clk	;	
	av_write 						      <=		av_master0_write;			
	av_writedata				      <=	  av_master0_writedata;
	av_read							      <=		av_master0_read;			
	av_master0_readdata	      <=  	av_readdata  ;
	av_address					      <=    av_master0_address;
  av_master0_readdatavalid  <=    av_valid;
  av_master0_waitrequest    <=    av_waitrequest  ;
		 			
  I2C_SCLK <= scl_o when scl_oen = '0' else 'Z';
  I2C_SDAT <= sda_o when sda_oen = '0' else 'Z';
  scl_i <= I2C_SCLK;
  sda_i <= I2C_SDAT;  

  -- LED(7 downto 0) <= pio_0(7 downto 0);

end rtl;
