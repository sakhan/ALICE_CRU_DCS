library ieee;
use ieee.std_logic_1164.all;
--use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity avalon_interface is
port
 ( 
  --================
    -- CLK & RST
  --================  
  av_clk        : in std_logic;
  av_reset      : in std_logic ;
  nReset        : in std_logic;
  --===============================================
    -- Avalon Interface (Memory Mapped Slave)
  --=============================================== 
  av_write      : in std_logic ;
  av_writedata  : in std_logic_vector(31 downto 0) ;
  av_read       : in std_logic ;
  av_readdata   : out std_logic_vector(31 downto 0) := x"00000000" ;
  av_address    : in std_logic_vector(7 downto 0) ;
  av_valid      : out std_logic ;
  av_waitrequest: out std_logic;
  --===================
    -- I2C interface
  --===================
  sda_i         : in std_logic;
  sda_o         : out std_logic;
  sda_oen       : out std_logic;
  scl_i         : in std_logic;
  scl_o         : out std_logic;
  scl_oen       : out std_logic
 
  );
  end avalon_interface;
  
  architecture structural of avalon_interface is
  
  component i2c_master_eeprom is
  port (
    --================
    -- CLK & RST
    --================
    clk         : in std_logic;
    rst         : in std_logic;
    nReset      : in std_logic;
    --=================
    -- I2C commands
    --=================
    start_wr_i  : in std_logic;
    addr_i      : in std_logic_vector(7 downto 0);
    data_i      : in std_logic_vector(7 downto 0);
    start_rd_i  : in std_logic;
    done_o      : out std_logic;
    data_o      : out std_logic_vector(7 downto 0);
    ack_out     : inout std_logic;
    slave_addr  : in std_logic_vector(7 downto 0);
    --===============================
    -- Test ack
    --===============================
    ack_WR0     : out std_logic;
    ack_RD0     : out std_logic;
    i2c_busy    : out std_logic; 
    idle_bit    : out std_logic;
    --=================
    -- I2C interface
    --=================
    sda_i       : in std_logic;
    sda_o       : out std_logic;
    sda_oen     : out std_logic;
    scl_i       : in std_logic;
    scl_o       : out std_logic;
    scl_oen     : out std_logic
    );
end component i2c_master_eeprom;

  signal start_wr_i     : std_logic ; 
  signal start_rd_i     : std_logic ;
  signal addr_i         : std_logic_vector(7 downto 0);
  --signal writedata_regs : std_logic_vector(31 downto 0) ;
  signal readdata_regs  : std_logic_vector (31 downto 0):= x"00000000"; 
  signal data_i         : std_logic_vector(7 downto 0):= x"00";
  signal done_o         : std_logic ;
  signal data_o         : std_logic_vector (7 downto 0) := x"00";
  signal ack_out        : std_logic;
  signal slave_addr     : std_logic_vector(7 downto 0);
  signal ack_RD0        : std_logic;
  signal ack_WR0        : std_logic;
  signal i2c_busy       : std_logic;
  signal idle_bit       : std_logic;


begin

 --Avalon-MM Slave - Write Access 
  p_av_write : process (av_clk)
  begin
    if rising_edge(av_clk) then   
      start_wr_i  <=  '0';
      start_rd_i  <=  '0';
      if (av_write = '1') then  
        if  (av_address = x"00")  then 
          slave_addr            <=  av_writedata(7 downto 0);
          data_i(7 downto 0)    <=  av_writedata(15 downto 8);  
          addr_i(7 downto 0)    <=  av_writedata(23 downto 16);
          start_wr_i            <=  av_writedata(24)  ;
          start_rd_i            <=  av_writedata(25)  ;   
        else
          slave_addr            <=  x"00";
          data_i                <=  x"00";
          addr_i                <=  x"00";
          start_wr_i            <=  '0';
          start_rd_i            <=  '0';
        end if;
      end if;
    end if;
  end process;
  
  -- Avalon-MM Slave - Read Access
  p_av_read : process (av_clk)
  begin 
    if  rising_edge(av_clk) then
      av_valid <= '0';
      if  av_read = '1' then
      
       if (av_address  = x"00") then 
          av_valid  <= '1';
          av_readdata(7 downto 0) <= data_o;
          av_readdata(8)          <= done_o;
          av_readdata(9)          <= ack_out;
          av_readdata(10)         <= ack_WR0;
          av_readdata(11)         <= ack_RD0;
          av_readdata(12)         <= idle_bit;
        else 
          av_readdata <= (others => '0');
        end if; 
        --av_readdata <= readdata_regs;
      end if;
     
    end if;
  end process;
  
  
i2c_master : i2c_master_eeprom 
port map (
    --================
    -- CLK & RST
    --================
      clk         =>  av_clk,     --clk     
      rst         =>  av_reset,   --rst     
      nReset      =>  nReset,     
      --============        
      -- I2C commands       
      --============        
      start_wr_i  =>  start_wr_i, 
      start_rd_i  =>  start_rd_i,   
      done_o      =>  done_o,
      addr_i      =>  addr_i,     
      data_i      =>  data_i,                       
      data_o      =>  data_o, 
      ack_out     =>  ack_out,
      slave_addr  =>  slave_addr,
     --======================== 
      ack_WR0     =>  ack_WR0,
      ack_RD0     =>  ack_RD0,
      i2c_busy    =>  i2c_busy,
      idle_bit    =>  idle_bit,
      --============          
      -- I2C interface
      --============
      sda_i       =>  sda_i,  
      sda_o       =>  sda_o,    
      sda_oen     =>  sda_oen, 
      scl_i       =>  scl_i,  
      scl_o       =>  scl_o,  
      scl_oen     =>  scl_oen 
    );
      
end structural;  
  


 


  
  
  
  
  
  
  
  
  -- 31 downto 0
  
  -- 1 bit for wr 
  -- writedata 32 bit source 
  -- two 8 bit addr , data. start write 1 bit read 1 bit 
  -- readdata  8 bit data probe, 1 bit for done_o
  -- scanning feature ack etc.  