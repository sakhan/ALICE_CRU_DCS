library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all; -- we should use ieee.numeric_std.all
--use ieee.numeric_std.all;    ------- preferred
use ieee.std_logic_unsigned.all;

entity i2c_master_eeprom is
  port (
    --================
    -- CLK & RST
    --================
    clk 				: in std_logic;
    rst 				: in std_logic;
    nReset 			: in std_logic;
    --=================
    -- I2C commands
    --=================
    start_wr_i 	: in std_logic;
    addr_i 			: in std_logic_vector(7 downto 0);
    data_i 			: in std_logic_vector(7 downto 0);
    done_o 			: out std_logic;
    start_rd_i 	: in std_logic;
    data_o 			: out std_logic_vector(7 downto 0);
		ack_out			:	out std_logic;
		slave_addr	: in std_logic_vector(7 downto 0); --:= x"A0";
    --=================
    -- I2C interface
    --=================
    sda_i 			: in std_logic;
    sda_o 			: out std_logic;
    sda_oen 		: out std_logic;
    scl_i 			: in std_logic;
    scl_o 			: out std_logic;
    scl_oen 		: out std_logic
    );
end i2c_master_eeprom;

architecture structural of i2c_master_eeprom is
		--signal slave_addr			:	std_logic_vector(7 downto 0)	:= x"A0";
 -- ADDR
  --constant SLV_ADD_W 		: std_logic_vector(7 downto 0) 	:= x"A0"; -- SLV ADD + W bit   --- A to B for re-check
  --constant SLV_ADD_R 		: std_logic_vector(7 downto 0) 	:= x"A1"; -- SLV ADD + R bit
  --constant PRESCALER 		: unsigned(15 downto 0) 				:= X"0018"; -- 50 MHz input, 400 KHz output  --- for Silicon
	constant PRESCALER 			: unsigned(15 downto 0) := X"0005"; -- 50 MHz input, 400 KHz output  -- only for simulation
	
  -- SIGNALs
	signal core_en 						: std_logic; 
	signal start_en						:	std_logic;  
	signal wr_en							:	std_logic;
	signal rd_en							:	std_logic;
	signal ack_in_en 					: std_logic ;
  signal din_en,dout				: std_logic_vector (7 downto 0);
	signal data_o_temp 				: std_logic_vector (7 downto 0) := x"00";
	signal stop_en						: std_logic;
  signal i2c_done 					:	std_logic := '0';
  signal i2c_al 						: std_logic;

	
	type i2c_state is (
    IDLE,
		WR0, WR1, WR2,
		RD0, RD1, RD2, RD3,
    DONE);
  signal s_cs, s_ns  				: i2c_state;   



begin  -- structural
  
  ----------------
  -- I2C interface
  ----------------

  i_i2c_master_byte_ctrl 		: entity work.i2c_master_byte_ctrl
    port map (
      clk 			=> clk,
      rst 			=> rst,
      nReset 		=> nReset,
      ena 			=> core_en,
      clk_cnt		=> PRESCALER,
			start			=> start_en,							--- edited
      stop			=> stop_en,              	--- edited
      read 			=> rd_en,  								--- edited
      write 		=> wr_en,									--- edited
      ack_in 		=> ack_in_en,							--- edited
      din 			=> din_en,
      cmd_ack 	=> i2c_done,
      ack_out		=> ack_out,
      dout 			=> dout,
      i2c_busy 	=> open,
      i2c_al 		=> i2c_al,
      scl_i 		=> scl_i,
      scl_o 		=> scl_o,
      scl_oen 	=> scl_oen,
      sda_i 		=> sda_i,
      sda_o 		=> sda_o,
      sda_oen 	=> sda_oen
      );
			core_en <= not rst;
			
	p_sync: process(clk)
	begin
	if rising_edge(clk) then
		if (rst = '1') then
			--data_o_temp 	<= x"00";
			--done_o	<= '0';
			s_cs		<= IDLE;
		else 
			s_cs 		<= s_ns; 
	  end if; 
	end if; 		
	end process p_sync;
	
	p_cmb: process(s_cs, start_wr_i,start_rd_i,i2c_done)
	begin
  start_en 	<= '0';
	rd_en  		<= '0';
	wr_en 		<= '0';
	ack_in_en <= '0';
	stop_en 	<= '0';
	
	s_ns 			<= s_cs;
	
		case s_cs is
			when IDLE =>
        if(start_wr_i = '0' and start_rd_i = '0') then
                s_ns 	<= IDLE;
        elsif (start_wr_i = '1'and start_rd_i = '1') then
                assert not (start_wr_i = '1'and start_rd_i = '1') report "Illegal code of operation" severity ERROR;
                s_ns  <=  IDLE;
				elsif (start_wr_i = '1' and start_rd_i = '0') then   -- start and going to next state i.e memory address to write
								s_ns 	<= WR0; 
				elsif(start_rd_i = '1' and start_wr_i = '0') then    -- start reading the data after idle, 2nd option	
								s_ns 	<= RD0;     -- going to next state i.e memory address to be read 	
				end if;
				
			when WR0 =>                -- state for memory address to be written
						done_o		<= '0';
				if i2c_done = '1' then
						s_ns			<= WR1;
					else  s_ns	<= WR0;
				end if;
						start_en 	<= '1';
						wr_en		 	<= '1';
					   
      when WR1 =>              -- writing data 	
						done_o		<= '0';
				if i2c_done = '1' then		
						s_ns 			<= WR2;
					else  s_ns	<= WR1;
				end if;	
						wr_en 		<= '1';
 
      when WR2 =>
						done_o		<= '0';		
				if i2c_done = '1' then		
						s_ns 			<= DONE;
					else  s_ns	<= WR2;
				end if;	
						wr_en 		<= '1';
						stop_en 	<= '1';				
				
			When RD0 =>              --  writing the memory address to be read 
						done_o		<= '0';
				if i2c_done = '1' then 		
						s_ns 			<= RD1;
					else  s_ns	<= RD0;
				end if;	
						start_en 	<= '1';
						wr_en 		<= '1';
					 				
      When RD1	=>                 -- reading the data from the slave 
						done_o		<= '0';
				if i2c_done = '1' then			
						s_ns 			<= RD2;
					else  s_ns	<= RD1;
				end if;	 
						wr_en 		<= '1';   
	
      When RD2	=>                 -- reading the data from the slave 
						done_o		<= '0';
				if i2c_done = '1' then		
						s_ns	 		<= RD3;
					else  s_ns	<= RD2;
				end if;			
						start_en	<= '1';
						wr_en 		<= '1';

			when RD3 =>
						done_o		<= '0';	
				if i2c_done = '1' then		
						s_ns			<= DONE;
					else  s_ns	<= RD3;
				end if;	
						rd_en  		<= '1';
						ack_in_en <= '1';  
						stop_en 	<= '1';		
							
			when DONE => 
						done_o		<= '1';	
				if(start_wr_i = '0' and start_rd_i = '0') then
						s_ns 			<= IDLE;
					--else  s_ns		<= DONE;
        end if;
        if (start_wr_i = '1'and start_rd_i = '1') then
            assert not (start_wr_i = '1'and start_rd_i = '1') report "Illegal code of operation" severity ERROR;
            s_ns  <=  DONE;
        end if;   
				if (start_wr_i = '1' and start_rd_i = '0') then   
						s_ns 	<= Idle; 
        end if;
				if(start_rd_i = '1' and start_wr_i = '0') then    
						s_ns 	<= Idle ;     
				--	else  s_ns	<= IDLE;
				end if;	
						
		end case;
		
	end process p_cmb;
	
	p_reg_out : process(s_cs)
  begin
          
  -- if rising_edge(clk) then		 
				case s_cs is
				when WR0 =>
				din_en <= slave_addr(7 downto 1) & '0'; -- SLV_ADD_W;  
				
				when WR1 =>
				din_en <= addr_i;
				
				when WR2 =>
				din_en <= data_i;
				 
				when RD0 =>
				din_en <= slave_addr(7 downto 1) & '0';		--SLV_ADD_W;	
				
				when RD1 =>
				din_en <= addr_i;

				when RD2 =>
				din_en <= slave_addr(7 downto 1) & '1';	--SLV_ADD_R

				when RD3 =>
			--	data_o_temp <= dout;
				
				when IDLE =>
				din_en <= x"00";
				
				
				when DONE =>
				din_en <= x"00";
					
			end case;
--  end if;
	end process p_reg_out;
	
	-- p_synchronous : process (clk)
	 -- begin 
	  -- if rising_edge(clk) then

		 -- din 		<= din_en;
		 -- start 	<= start_en;  
		 -- rd  		<= rd_en;
		 -- wr 			<= wr_en;
		 -- ack_in 	<= ack_in_en;  
		 -- stop 		<= stop_en;
		 
		-- end if;
	-- end process p_synchronous;	 
		  
	
	p_out : process (clk)
	 begin
	  if rising_edge(clk) then
			  if (s_cs = RD3) then
							data_o_temp	<= 	dout;
				end if;  
              data_o <= data_o_temp;						
		end if;
	 end process p_out;
	
end structural;

